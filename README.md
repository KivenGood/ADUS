# ADUS

#### 介绍
使用Node.js+vue.js实现的漏洞库的增删改查；

#### 软件架构
Node.js+vue.js


#### 安装教程

1.安装Node.js环境，推荐安装完后再安装cnpm镜像，否则有些包不好下载。
2.使用命令cnpm install安装包；
3.添加数据库；
4.在config.js中修改参数；
5.使用命令cnpm start启动项目  